//
//  CommunicationInfo.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/19.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserInfomation.h"

@interface CommunicationInfo : RLMObject

@property UserInfomation *user;
@property NSDate *sendDate;
@property NSDate *receiveDate;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<CommunicationInfo>
RLM_ARRAY_TYPE(CommunicationInfo)
