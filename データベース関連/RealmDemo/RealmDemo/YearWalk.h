//
//  YearWalk.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/12.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserInfomation.h"

@interface YearWalk : RLMObject

@property NSInteger yearWalkID;
@property UserInfomation *user;
@property NSDate *date;
@property float stride;
@property float speed;
@property NSInteger count;
@property NSDate *updateTime;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<YearWalk>
RLM_ARRAY_TYPE(YearWalk)
