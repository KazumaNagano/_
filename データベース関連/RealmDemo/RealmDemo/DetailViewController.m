//
//  DetailViewController.m
//  RealmDemo
//
//  Created by kazuma_n on 2016/04/30.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import "DetailViewController.h"
#import "HealthCondition.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy年 MM/dd"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        self.detailDescriptionLabel.text = [formatter stringFromDate:[self.detailItem birthday]];
        self.heightLabel.text = [NSString stringWithFormat:@"%@ cm",@([self.detailItem height]) ];
        self.weightLabel.text = [NSString stringWithFormat:@"%@ kg",@([self.detailItem weight])];
        
        HealthCondition *health = [[HealthCondition objectsWhere:[NSString stringWithFormat:@"user.userID == %@",@(self.detailItem.userID)]] firstObject];
        if (health) {
            self.healthStageLabel.text = [@"健康状態:                             " stringByAppendingString: health.healthMessage];
            self.riskLabal.text = [@"健康リスク:    " stringByAppendingString:health.risk];
        }

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
