//
//  WeekWalk.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/12.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserInfomation.h"

@interface WeekWalk : RLMObject

@property NSInteger weekWalkID;
@property UserInfomation *user;
@property NSDate *date;
@property float stride;
@property float speed;
@property NSDate *updateTime;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<WeekWalk>
RLM_ARRAY_TYPE(WeekWalk)
