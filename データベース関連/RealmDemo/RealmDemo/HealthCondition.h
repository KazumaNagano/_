//
//  HealthCondition.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/03.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserInfomation.h"

@interface HealthCondition : RLMObject

@property NSInteger healthConditionID;
@property UserInfomation *user;
@property NSInteger goalStep;
@property NSString *risk;
@property NSString *chatMessage;
@property BOOL existsNewChat;
@property float averageSpeed;
@property float averageStride;
@property float averageWalkAge;
@property BOOL healthCondition;
@property NSString *healthMessage;
@property float walkspeed;
@property float walkstride;
@property float walkAge;
@property BOOL speedChange;
@property NSString *speedChangeMessage;
@property NSDate *updateTime;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<HealthCondition>
RLM_ARRAY_TYPE(HealthCondition)
