//
//  MasterData.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/03.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfomation.h"
#import "HealthCondition.h"
#import "EachTimeWalk.h"
#import "DayWalk.h"
#import "weekWalk.h"
#import "MonthWalk.h"
#import "QuarterWalk.h"
#import "YearWalk.h"
#import "MimamoriRelation.h"
#import "CommunicationInfo.h"

@interface MasterData : NSObject

+ (id)sharedManager;
- (void)loadAll;

@end
