//
//  Walking.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/03.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserInfomation.h"

@interface EachTimeWalk : RLMObject

@property NSInteger walkID;
@property UserInfomation *user;
@property NSDate *date;
@property NSInteger hour;
@property NSInteger count;
@property NSDate *updateTime;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<Walking>
RLM_ARRAY_TYPE(Walking)
