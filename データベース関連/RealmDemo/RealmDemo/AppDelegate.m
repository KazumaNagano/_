//
//  AppDelegate.m
//  RealmDemo
//
//  Created by kazuma_n on 2016/04/30.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import "AppDelegate.h"
#import "DetailViewController.h"
#import <Realm/Realm.h>
#import "MasterData.h"

@interface AppDelegate () <UISplitViewControllerDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
    UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem;
    splitViewController.delegate = self;
    
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    // 新しいスキーマバージョンを設定します。以前のバージョンより大きくなければなりません。
    // （スキーマバージョンを設定したことがなければ、最初は0が設定されています）
    config.schemaVersion = 1;
    
    // マイグレーション処理を記述します。古いスキーマバージョンのRealmを開こうとすると
    // 自動的にマイグレーションが実行されます。
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // 最初のマイグレーションの場合、`oldSchemaVersion`は0です
        if (oldSchemaVersion < 1) {
            // 何もする必要はありません！
            // Realmは自動的に新しく追加されたプロパティと、削除されたプロパティを認識します。
            // そしてディスク上のスキーマを自動的にアップデートします。
        }
    };
    
    // Tell Realm to use this new configuration object for the default Realm
    [RLMRealmConfiguration setDefaultConfiguration:config];
    
    // Now that we've told Realm how to handle the schema change, opening the file
    // will automatically perform the migration
    [RLMRealm defaultRealm];
    [[MasterData sharedManager] loadAll];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[DetailViewController class]] && ([(DetailViewController *)[(UINavigationController *)secondaryViewController topViewController] detailItem] == nil)) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    } else {
        return NO;
    }
}

@end
