//
//  MasterData.m
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/03.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import "MasterData.h"
#import <Realm/Realm.h>

static id masterData = nil;
static id realm = nil;

@implementation MasterData{
    NSBundle *bundle;
}

+ (id)sharedManager { // これはクラスメソッド(+つきで定義)
    if (masterData == nil) { // もし初期化を経ていなければ
        masterData = [[self alloc] init]; // 作成・初期化 
        realm = RLMRealm.defaultRealm;
        [realm beginWriteTransaction];
        [realm deleteAllObjects];
        [realm commitWriteTransaction];
        return masterData;
    }
    
    return masterData;
}


- (void)loadDefaultRealm{
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *v0URL = [[NSBundle mainBundle] URLForResource:@"default-v1" withExtension:@"realm"];
    [[NSFileManager defaultManager] removeItemAtURL:defaultRealmURL error:nil];
    [[NSFileManager defaultManager] copyItemAtURL:v0URL toURL:defaultRealmURL error:nil];
    realm = RLMRealm.defaultRealm;
}

- (void)loadAll{
    bundle =[NSBundle mainBundle];
    
    /* default.realmをダウンロード
    NSURL *defaultRealmURL = [RLMRealmConfiguration defaultConfiguration].fileURL;
    NSURL *v0URL = [[NSBundle mainBundle] URLForResource:@"default-v1" withExtension:@"realm"];
    [[NSFileManager defaultManager] removeItemAtURL:defaultRealmURL error:nil];
    [[NSFileManager defaultManager] copyItemAtURL:v0URL toURL:defaultRealmURL error:nil];
    realm = RLMRealm.defaultRealm;
    */
    [self loadUserInfomation];
    [self loadHealthCondition];
    [self loadEachTimeWalk];
    [self loadDayWalk];
    [self loadWeekWalk];
    [self loadMonthWalk];
    [self loadQuarterWalk];
    [self loadYearWalk];
    [self loadMimamoriRelation];
    [self loadCommunicationInfo];

}

- (void)loadUserInfomation{
    if (![UserInfomation allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"UserInfomation" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておく
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyyMMdd"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[4]];
                
                [UserInfomation createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],
                                                                [NSNumber numberWithInteger:[array[1] integerValue]],array[2],array[3],date,
                                                                [NSNumber numberWithInteger:[array[5] integerValue]],
                                                                [NSNumber numberWithInteger:[array[6] integerValue]],array[7],array[8],array[9],
                                                                [NSNumber numberWithInteger:[array[10] integerValue]],
                                                                [NSNumber numberWithInteger:[array[11] integerValue]]]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadHealthCondition{
    if (![HealthCondition allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"HealthCondition" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *update = [formatter dateFromString:array[16]];
                
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                [HealthCondition createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,
                                                                 [NSNumber numberWithInteger:[array[2] integerValue]],array[3],array[4],
                                                                 [NSNumber numberWithBool:[array[5] integerValue]],
                                                                 [NSNumber numberWithFloat:[array[6] floatValue]],
                                                                 [NSNumber numberWithFloat:[array[7] floatValue]],
                                                                 [NSNumber numberWithFloat:[array[8] floatValue]],
                                                                 [NSNumber numberWithBool:[array[9] integerValue]],array[10],
                                                                 [NSNumber numberWithFloat:[array[11] floatValue]],
                                                                 [NSNumber numberWithFloat:[array[12] floatValue]],
                                                                 [NSNumber numberWithFloat:[array[13] floatValue]],
                                                                 [NSNumber numberWithBool:[array[14] integerValue]],array[15],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadEachTimeWalk{
    if (![EachTimeWalk allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"EachTimeWalk" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[2]];
                
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                NSDate *update = [formatter dateFromString:array[5]];
                
                [EachTimeWalk createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,date,
                                                                 [NSNumber numberWithInteger:[array[3] integerValue]],
                                                                 [NSNumber numberWithInteger:[array[4] integerValue]],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadDayWalk{
    if (![DayWalk allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"DayWalk" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[2]];
                
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                NSDate *update = [formatter dateFromString:array[5]];
                
                [DayWalk createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,date,
                                                      [NSNumber numberWithFloat:[array[3] floatValue]],
                                                      [NSNumber numberWithFloat:[array[4] floatValue]],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadWeekWalk{
    if (![WeekWalk allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"WeekWalk" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[2]];
                
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                NSDate *update = [formatter dateFromString:array[5]];
                
                [WeekWalk createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,date,
                                                         [NSNumber numberWithFloat:[array[3] floatValue]],
                                                         [NSNumber numberWithFloat:[array[4] floatValue]],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadMonthWalk{
    if (![MonthWalk allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"MonthWalk" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[2]];
                
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                NSDate *update = [formatter dateFromString:array[7]];
                
                [MonthWalk createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,date,
                                                           [NSNumber numberWithFloat:[array[3] floatValue]],
                                                           [NSNumber numberWithFloat:[array[4] floatValue]],
                                                           [NSNumber numberWithInteger:[array[5] integerValue]],
                                                           [NSNumber numberWithFloat:[array[6] floatValue]],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadQuarterWalk{
    if (![QuarterWalk allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"QuarterWalk" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[2]];
                
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                NSDate *update = [formatter dateFromString:array[6]];
                
                [QuarterWalk createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,date,
                                                           [NSNumber numberWithFloat:[array[3] floatValue]],
                                                           [NSNumber numberWithFloat:[array[4] floatValue]],
                                                           [NSNumber numberWithInteger:[array[5] integerValue]],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}


- (void)loadYearWalk{
    if (![YearWalk allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"YearWalk" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *date = [formatter dateFromString:array[2]];
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                NSDate *update = [formatter dateFromString:array[6]];
                
                [YearWalk createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],user,date,
                                                          [NSNumber numberWithFloat:[array[3] floatValue]],
                                                          [NSNumber numberWithFloat:[array[4] floatValue]],
                                                          [NSNumber numberWithInteger:[array[5] integerValue]],update]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadCommunicationInfo{
    if (![CommunicationInfo allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"CommunicationInfo" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            // 保存先のパスを出力しておくs
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                
                NSString *searchText = [NSString stringWithFormat:@" userID = %d",[array[0] intValue]];
                UserInfomation *user = [[UserInfomation objectsWhere:searchText] firstObject];
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                //タイムゾーンの指定
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *send= [formatter dateFromString:array[1]];
                NSDate *receive = [formatter dateFromString:array[2]];
                
                [CommunicationInfo createInRealm:realm withValue:@[user,send,receive]];
                
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

- (void)loadMimamoriRelation{
    if (![MimamoriRelation allObjects].count){
        [realm beginWriteTransaction];
        NSString *csvPath = [bundle pathForResource:@"MimamoriRelation" ofType: @"csv"];
        if  (csvPath){
            NSString *csvString= @"";
            NSData *csvData = [[NSData alloc] init];
            @try{
                csvData = [NSData dataWithContentsOfFile:csvPath];
                csvString = [[NSString alloc] initWithData:csvData encoding:NSShiftJISStringEncoding];
            }
            @catch(NSError *error) {
                NSLog(@"%@",error.localizedDescription);
            }
            NSScanner *scanner = [NSScanner scannerWithString:csvString];
            // 改行文字の選定
            NSCharacterSet *chSet = [NSCharacterSet newlineCharacterSet];            // 保存先のパスを出力しておくs
            NSString *line;
            [scanner scanUpToCharactersFromSet:chSet intoString:NULL];
            [scanner scanCharactersFromSet:chSet intoString:NULL];
            
            NSLog(@"%@",[RLMRealmConfiguration defaultConfiguration].fileURL);
            while (![scanner isAtEnd]) {
                [scanner scanUpToCharactersFromSet:chSet intoString:&line];
                NSArray *array = [line componentsSeparatedByString:@","];
                NSString *searchText1 = [NSString stringWithFormat:@" userID = %d",[array[1] intValue]];
                UserInfomation *sender = [[UserInfomation objectsWhere:searchText1] firstObject];
                
                NSString *searchText2 = [NSString stringWithFormat:@" userID = %d",[array[2] intValue]];
                UserInfomation *receiver = [[UserInfomation objectsWhere:searchText2] firstObject];
                
                NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                NSDate *update = [formatter dateFromString:array[6]];
                
                [MimamoriRelation createInRealm:realm withValue:@[[NSNumber numberWithInteger:[array[0] integerValue]],sender,receiver,array[3],array[4],array[5],update]];
                // 改行文字をスキップ
                [scanner scanCharactersFromSet:chSet intoString:NULL];
            }
        }
        [realm commitWriteTransaction];
    }
}

@end
