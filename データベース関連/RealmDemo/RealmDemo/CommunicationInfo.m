//
//  CommunicationInfo.m
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/19.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import "CommunicationInfo.h"

@implementation CommunicationInfo

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
