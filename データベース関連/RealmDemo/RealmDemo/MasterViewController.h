//
//  MasterViewController.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/04/30.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

