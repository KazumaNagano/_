//
//  DetailViewController.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/04/30.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfomation.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) UserInfomation *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *healthStageLabel;
@property (weak, nonatomic) IBOutlet UILabel *riskLabal;


@end

