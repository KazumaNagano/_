//
//  UserInfomation.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/03.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>

@interface UserInfomation : RLMObject

@property NSInteger userID;
@property NSInteger terminalID;
@property NSString *name;
@property NSString *sex;
@property NSDate *birthday;
@property NSInteger height;
@property NSInteger weight;
@property NSString *postageCode;
@property NSString *iconId;
@property NSString *password;
@property NSInteger sumPoint;
@property NSInteger numberOfCoupons;

@end


// This protocol enables typed collections. i.e.:
// RLMArray<UserInfomation>
RLM_ARRAY_TYPE(UserInfomation)
