//
//  MimamoriRelation.h
//  RealmDemo
//
//  Created by kazuma_n on 2016/05/03.
//  Copyright © 2016年 kazuma_n. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserInfomation.h"

@interface MimamoriRelation : RLMObject

@property NSInteger mimamoriID;
@property UserInfomation *observer;
@property UserInfomation *monitor;
@property NSString *attribute;
@property NSString *houseCondition;
@property NSString *alarm;
@property NSDate *updateTime;


@end

// This protocol enables typed collections. i.e.:
// RLMArray<MimamoriRelation>
RLM_ARRAY_TYPE(MimamoriRelation)
