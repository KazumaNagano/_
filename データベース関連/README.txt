

□　Realm
・iOS 7以降、Xcode 6.4以降。
・Objective‑C、swiftをサポート。また、アンドロイドとも互換性があります。
・	使い勝手が良く、簡単かつ動作が速いことで今回Realmを選びました。
・コーディング方法についてはデータベース規約に従ってください。

□サンプルプログラム
・公式サンプル
	公式のサンプルです。
	
・RealmDemo
	zipファイルが同梱されています。事前に用意したRealmファイルを読み込みテーブルにデータの一部を表示するサンプルを作成しました。
	データのアクセス方法、realmファイルの読み込み方等はMasterDataクラス、
	RLMObject(テーブルのようなもの)の宣言はRLMClassグループを参考にしてください。

・default-v1.realm
こちらで作成したサンプルデータが入ったrealmファイルです。
導入方法はデータベース規約"realmファイルの読み込み"を参照してください。

□	参考資料
・　API Reference　https://realm.io/docs/objc/latest/api/
・	realm公式のサンプル
最新版のRealmのexamplesフォルダにiOSのサンプルコードがあります。 マイグレーション、UITableViewControllerとの使い方、暗号化やコマンドラインツールなど、さまざまな機能が紹介されています。
・	公式ドキュメントhttps://realm.io/jp/docs/objc/latest/
